const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().functions);

var newData;

exports.messageTrigger = functions.firestore.document('notifications/{notificationId}').onCreate(async (snapshot, context)=>{
    if (snapshot.empty) {
        console.log('No Devices');
        return;
    }

    var tokens = [];

    newData = snapshot.data();

    const deviceTokens = await admin.firestore().collection('devices').get();

    for (var token of deviceTokens.docs) {
        tokens.push(token.data().device_token);
    }

    var payload = {
      notification : {title: newData.title, body: newData.body, android_channel_id:'notification_heads_up'},
      data: {click_action: 'FLUTTER_NOTIFICATION_CLICK', message: newData.message, view: newData.view, postId: newData.postId, title: newData.title, body: newData.body}
    };

    try {
        const response = await admin.messaging().sendToDevice(tokens, payload);
        console.log('Notification sent successfully');
    } catch (err) {
        console.log('Error sending notification');
    }
});


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

