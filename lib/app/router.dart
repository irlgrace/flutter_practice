import 'package:auto_route/auto_route_annotations.dart';
import 'package:flutterpractice/ui/views/home/home_view.dart';
import 'package:flutterpractice/ui/views/login/login_view.dart';
import 'package:flutterpractice/ui/views/notification/notification_view.dart';
import 'package:flutterpractice/ui/views/post/post_view.dart';
import 'package:flutterpractice/ui/views/post/singlepost_view.dart';
import 'package:flutterpractice/ui/views/signup/signup_view.dart';
import 'package:flutterpractice/ui/views/startup/startup_view.dart';

@MaterialAutoRouter()
class $Router {
  @initial
  StartUpView startUpViewRoute;

  HomeView homeViewRoute;

  SignUpView signUpViewRoute;

  LoginView loginViewRoute;

  PostView postViewRoute;

  NotificationView notificationViewRoute;

  SinglePostView singlePostViewRoute;
}