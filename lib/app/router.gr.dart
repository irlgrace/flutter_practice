// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutterpractice/models/notification.dart';
import 'package:flutterpractice/models/post.dart';
import 'package:flutterpractice/ui/views/login/login_view.dart';
import 'package:flutterpractice/ui/views/notification/notification_view.dart';
import 'package:flutterpractice/ui/views/post/post_view.dart';
import 'package:flutterpractice/ui/views/post/singlepost_view.dart';
import 'package:flutterpractice/ui/views/startup/startup_view.dart';
import 'package:flutterpractice/ui/views/home/home_view.dart';
import 'package:flutterpractice/ui/views/signup/signup_view.dart';

abstract class Routes {
  static const startUpViewRoute = '/';
  static const homeViewRoute = '/home-view-route';
  static const signUpViewRoute = '/sign-up-route';
  static const loginViewRoute = '/login-route';
  static const postViewRoute = '/post-route';
  static const notificationViewRoute = '/notification-route';
  static const singlePostViewRoute = '/single-post-route';
  static const all = {
    startUpViewRoute,
    homeViewRoute,
    signUpViewRoute,
    loginViewRoute,
    postViewRoute,
    notificationViewRoute,
    singlePostViewRoute
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.startUpViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => StartUpView(),
          settings: settings,
        );
      case Routes.homeViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => HomeView(),
          settings: settings,
        );
      case Routes.signUpViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SignUpView(),
          settings: settings,
        );
      case Routes.loginViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => LoginView(),
          settings: settings,
        );
      case Routes.postViewRoute:
        var postToEdit = settings.arguments as Post;
        return MaterialPageRoute<dynamic>(
          builder: (context) => PostView(
            editingPost: postToEdit
          ),
          settings: settings,
        );
      case Routes.notificationViewRoute:
        var notificationsToShow = settings.arguments as List<PushNotification>;
        return MaterialPageRoute<dynamic>(
          builder: (context) => NotificationView(
            notifications: notificationsToShow
          ),
          settings: settings,
        );
      case Routes.singlePostViewRoute:
        var postId = settings.arguments as String;
        return MaterialPageRoute<dynamic>(
          builder: (context) => SinglePostView(
              postId: postId
          ),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}
