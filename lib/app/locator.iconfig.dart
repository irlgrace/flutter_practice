// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:flutterpractice/services/authentication_service.dart';
import 'package:flutterpractice/services/cloud_storage_service.dart';
import 'package:flutterpractice/services/firestore_service.dart';
import 'package:flutterpractice/services/push_notification_service.dart';
import 'package:flutterpractice/services/third_party_services_module.dart';
import 'package:flutterpractice/utils/image_selector.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final thirdPartyServicesModule = _$ThirdPartyServicesModule();
  g.registerLazySingleton<DialogService>(
      () => thirdPartyServicesModule.dialogService);
  g.registerLazySingleton<NavigationService>(
      () => thirdPartyServicesModule.navigationService);
  g.registerLazySingleton<AuthenticationService>(
          () => thirdPartyServicesModule.authenticationService);
  g.registerLazySingleton<FirestoreService>(
          () => thirdPartyServicesModule.firestoreService);
  g.registerLazySingleton<CloudStorageService>(
          () => thirdPartyServicesModule.cloudStorageService);
  g.registerLazySingleton<ImageSelector>(
          () => thirdPartyServicesModule.imageSelector);
  g.registerLazySingleton<PushNotificationService>(
          () => thirdPartyServicesModule.pushNotificationService);
}

class _$ThirdPartyServicesModule extends ThirdPartyServicesModule {
  @override
  DialogService get dialogService => DialogService();
  @override
  NavigationService get navigationService => NavigationService();
  @override
  AuthenticationService get authenticationService => AuthenticationService();
  @override
  FirestoreService get firestoreService => FirestoreService();
  @override
  CloudStorageService get cloudStorageService => CloudStorageService();
  @override
  ImageSelector get imageSelector => ImageSelector();
  @override
  PushNotificationService get pushNotificationService => PushNotificationService();
}
