import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutterpractice/models/device.dart';
import 'package:flutterpractice/models/user.dart';
import 'package:flutterpractice/models/post.dart';
import 'package:flutterpractice/models/notification.dart';
import 'dart:async';


class FirestoreService {
  final CollectionReference _usersCollectionReference = Firestore.instance.collection('users');
  final CollectionReference _postsCollectionReference = Firestore.instance.collection('posts');
  final CollectionReference _notificationsCollectionReference = Firestore.instance.collection('notifications');
  final StreamController<List<PushNotification>> _notificationController = StreamController<List<PushNotification>>.broadcast();
  final StreamController<List<Post>> _postsController = StreamController<List<Post>>.broadcast();
  final CollectionReference _devicesCollectionReference = Firestore.instance.collection('devices');


  Future createUser(User user) async {
    try {
      await _usersCollectionReference.document(user.id).setData(user.toJson());
    } catch (e) {
      return e.message;
    }
  }

  Future getUser(String uid) async {
    try {
      var userData = await _usersCollectionReference.document(uid).get();
      return User.fromData(userData.data);
    } catch (e) {
      return e.message;
    }
  }

  Future addPost(Post post) async {
    try {

      var uniqueId = await _postsCollectionReference.add(post.toJson());

      var returnVar = {
        'uniqueId' : uniqueId.documentID,
        'added' : true
      };

      return returnVar;

    } catch (e) {
      return e.toString();
    }
  }

  Future getPostsOnceOff() async {
    try {
      var postDocuments = await _postsCollectionReference.getDocuments();
      if (postDocuments.documents.isNotEmpty) {
        return postDocuments.documents.map((snapshot) => Post.fromData(snapshot.data, snapshot.documentID))
            .where((mappedItem) => mappedItem.title != null)
            .toList();
      }
    } catch (e) {
      if (e is PlatformException){
        return e.message;
      }
      return e.toString();
    }
  }

  Future getSpecificPost(documentId) async {
    try {
      var post = await _postsCollectionReference.document(documentId).get();
      if (post.exists) {
        return Post.fromData(post.data, post.documentID);
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }
      return e.toString();
    }
  }

  Future deletePost(String documentId) async {
    await _postsCollectionReference.document(documentId).delete();
  }

  Future updatePost(Post post) async {
    try {
      await _postsCollectionReference.document(post.documentId).updateData(post.toJson());
      return true;
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  Future addNotification(PushNotification notification) async {
    try {
      await _notificationsCollectionReference.add(notification.toJson());
      return true;
    } catch (e) {
      return e.toString();
    }
  }

  Stream listenToPostsRealTime() {
    _postsCollectionReference.snapshots().listen((postsSnapshot) {
      if (postsSnapshot.documents.isNotEmpty) {
        var posts = postsSnapshot.documents.map((snapshot) => Post.fromData(snapshot.data, snapshot.documentID))
            .where((mappedItem) => mappedItem.title != null)
            .toList();
        _postsController.add(posts);
      }
    });

    return _postsController.stream;
  }


  Stream listenToNotificationRealTime() {
    _notificationsCollectionReference.snapshots().listen((notificationsSnapshot) {
      if (notificationsSnapshot.documents.isNotEmpty) {
        var notifications = notificationsSnapshot.documents.map((snapshot) =>
            PushNotification.fromData(snapshot.data))
            .where((mappedItem) => mappedItem.title != null)
            .toList();
        _notificationController.add(notifications);
      }
    });

    return _notificationController.stream;
  }

  Future addDevice(Device device) async {
    try {
      var post = await _devicesCollectionReference.where('device_token', isEqualTo:device.deviceToken).getDocuments();
      if (post.documents.isEmpty) {
        await _devicesCollectionReference.add(device.toJson());
      }
      return true;
    } catch (e) {
      return e.toString();
    }
  }
}