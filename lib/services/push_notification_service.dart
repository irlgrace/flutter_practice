import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:flutterpractice/models/notification.dart';
import 'package:stacked_services/stacked_services.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  final NavigationService _navigationService = locator<NavigationService>();
  List<PushNotification> _pushNotifications;
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();


  Future initialise() async {

    //_pushNotifications = List<PushNotification>();

    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }
    var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    flutterLocalNotificationsPlugin.initialize(
        InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS),
        onSelectNotification: onSelectNotification);

    _fcm.configure(
      //When app is closed
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        serialiseAndNavigate(message);
      },
      //When app is in the background
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        serialiseAndNavigate(message);
      },
    );

    _createNotificationChannel();
  }

  Future<void> _createNotificationChannel() async {
    var androidNotificationChannel = AndroidNotificationChannel(
        'notification_heads_up',
        'Heads Up',
        'For Android device',
        importance: Importance.Max,
    );
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(androidNotificationChannel);
  }

  void serialiseAndNavigate(Map<String, dynamic> message) {
    var notificationData = message['data'];
    var view = notificationData['view'];
    if (view == 'single_post') {
      _navigationService.navigateTo(Routes.singlePostViewRoute, arguments: notificationData['postId']);
    }
      //If there's no  view it'll just open the app on the first view
  }

  Future getToken() async {
    String token = await _fcm.getToken();
    print('device: $token');
    return token;
  }

  Future _showNotification(
      FlutterLocalNotificationsPlugin notificationsPlugin, {
        @required String title,
        @required String body,
        @required NotificationDetails type,
        int id = 0,
        var payload
      }) => notificationsPlugin.show(id, title, body, type, payload:payload);

  Future showNotification(
      FlutterLocalNotificationsPlugin notificationsPlugin, {
        @required String title,
        @required String body,
        int id = 0,
        var payload
      }) => _showNotification(notificationsPlugin, title: title, body: body, type: _justshow, id: id, payload: payload);

  NotificationDetails get _justshow {
    final androidChannnelSpecifics = AndroidNotificationDetails(
        'your channel id',
        'your channel name',
        'your channel description',
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'ticker'
    );
    final iOSChannelSpecifics = IOSNotificationDetails();
    return NotificationDetails(androidChannnelSpecifics, iOSChannelSpecifics);
  }

  Future onSelectNotification(payload){
    _navigationService.navigateTo(Routes.singlePostViewRoute, arguments: payload);
  }


  Future setNotification(Map<String, dynamic> message) {
    final notification = message['notification'];
    final data = message['data'];


      PushNotification notif = PushNotification(
          title: notification['title'],
          body: notification['body'],
          view: data['view'],
          message: data['message'],
          postId: data['postId']
      );
      _pushNotifications.add(notif);

      print('done set state');
  }
}