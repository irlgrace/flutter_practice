import 'package:flutterpractice/services/cloud_storage_service.dart';
import 'package:flutterpractice/services/firestore_service.dart';
import 'package:flutterpractice/services/push_notification_service.dart';
import 'package:flutterpractice/utils/image_selector.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked_services/stacked_services.dart';
import 'authentication_service.dart';

@registerModule
abstract class ThirdPartyServicesModule {
  @lazySingleton
  NavigationService get navigationService;
  @lazySingleton
  DialogService get dialogService;
  @lazySingleton
  AuthenticationService get authenticationService;
  @lazySingleton
  FirestoreService get firestoreService;
  @lazySingleton
  CloudStorageService get cloudStorageService;
  @lazySingleton
  ImageSelector get imageSelector;
  @lazySingleton
  PushNotificationService get pushNotificationService;
}