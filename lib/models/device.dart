class Device {
  final String id;
  final String deviceToken;
  final String type;

  Device({this.id, this.deviceToken, this.type});

  Device.fromData(Map<String, dynamic> data)
      : id = data['id'],
        deviceToken = data['device_token'],
        type = data['type'];

  Map<String, dynamic> toJson() {
    return {
      'device_token' : deviceToken,
      'type' : type
    };
  }
}