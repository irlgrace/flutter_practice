import 'package:flutter/foundation.dart';

class Post {
  final String title;
  final String imageUrl;
  final String userId;
  final String documentId;
  final String imageFileName;

  Post({
    @required this.title,
    @required this.userId,
    this.imageUrl,
    this.documentId,
    this.imageFileName
  });

  Map<String, dynamic> toJson() {
    return {
      'title' : title,
      'imageUrl' : imageUrl,
      'imageFileName' : imageFileName,
      'userId': userId
    };
  }

  static Post fromData(Map<String, dynamic> data, String documentId) {
    if (data == null) return null;

    return Post(
      title: data['title'],
      imageUrl: data['imageUrl'],
      userId: data['userId'],
      documentId: documentId,
      imageFileName: data['imageFileName'],
    );
  }
}