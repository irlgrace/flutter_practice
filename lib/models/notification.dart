import 'package:flutter/foundation.dart';

class PushNotification {
  final String title;
  final String body;
  final String message;
  final String view;
  final String postId;

  PushNotification({
    @required this.title,
    @required this.body,
    this.message,
    this.view,
    this.postId
  });

  Map<String, dynamic> toJson() {
    return {
      'title' : title,
      'body' : body,
      'message' : message,
      'view': view,
      'postId' : postId
    };
  }

  static PushNotification fromData(Map<String, dynamic> data) {
    if (data == null) return null;

    return PushNotification(
      title: data['title'],
      body: data['body'],
      message: data['message'],
      view: data['view'],
      postId: data['postId']
    );
  }
}