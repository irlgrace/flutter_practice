import 'package:image_picker/image_picker.dart';

class ImageSelector {
  //final picker = ImagePicker();
  Future selectImage() async {
    //return await picker.getImage(source: ImageSource.gallery);
    return await ImagePicker.pickImage(source: ImageSource.gallery);
  }
  Future captureImage() async {
    //return await picker.getImage(source: ImageSource.camera);
    return await ImagePicker.pickImage(source: ImageSource.camera);
  }
}