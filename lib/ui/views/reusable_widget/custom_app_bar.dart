import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:flutterpractice/models/notification.dart';
import 'package:flutterpractice/services/authentication_service.dart';
import 'package:flutterpractice/services/push_notification_service.dart';
import 'package:stacked_services/stacked_services.dart';

class CustomAppBar extends StatefulWidget {
  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  final NavigationService _navigationService = locator<NavigationService>();
  final PushNotificationService _pushNotificationService = locator<PushNotificationService>();
  final AuthenticationService _authService = locator<AuthenticationService>();
  FirebaseMessaging _fcm = FirebaseMessaging();
  List<PushNotification> _pushNotifications;
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();
    _pushNotifications = List<PushNotification>();

    _fcm.configure(
      //When app is open
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        setNotification(message);
      }
    );
  }

  Future setNotification(Map<String, dynamic> message) {
    final notification = message['notification'];
    final data = message['data'];

    setState(() {
      PushNotification notif = PushNotification(
          title: notification['title'],
          body: notification['body'],
          view: data['view'],
          message: data['message'],
          postId: data['postId']
      );
      _pushNotifications.add(notif);

    });
    //
    _pushNotificationService.showNotification(flutterLocalNotificationsPlugin,
        title: message['notification']['title'], body: message['notification']['body'], payload:data['postId']
    );
  }

  @override
  Widget build(BuildContext context) {
    return new AppBar(
        title: Text('Flutter Practice'),
        actions: <Widget>[
          new Padding(padding: const EdgeInsets.all(10.0),
              child: new Container(
                  height: 150.0,
                  width: 30.0,
                  child: new GestureDetector(
                    onTap: () async {
                      await _navigationService.navigateTo(Routes.notificationViewRoute, arguments: _pushNotifications);
                      setState(() {
                        _pushNotifications.clear();
                      });
                    },
                    child: new Stack(
                      children: <Widget>[
                        new IconButton(
                          icon: new Icon(
                              Icons.notifications,
                              color: Colors.white
                          ),
                          onPressed: null,
                        ),
                        _pushNotifications == null || _pushNotifications.length == 0? new Container() :
                        new Positioned(
                            child: new Stack(
                              children: <Widget>[
                                new Icon(
                                    Icons.brightness_1,
                                    size: 20.0, color: Colors.indigo),
                                new Positioned(
                                    top: 3.0,
                                    right: 4.0,
                                    child: new Center(
                                      child: new Text(
                                        _pushNotifications.length.toString(),
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.w500
                                        ),
                                      ),
                                    )
                                ),
                              ],
                            )
                        ),

                      ],
                    ),
                  )
              )
          ),
          new IconButton(
              icon: Icon(Icons.power_settings_new),
              onPressed: () async {
                await _authService.signOut();
                _navigationService.replaceWith(Routes.loginViewRoute);
              }
          )
        ]
    );
  }
}
