import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final TextEditingController textController;
  final String text;
  final Color textColor;
  final Color bottomBorderColor;
  final String textType;

  InputField({this.textController, this.text, this.textColor, this.bottomBorderColor, this.textType});

  @override
  Widget build(BuildContext context) {
    bool isPassword = textType == 'password' ? true: false;

    return Container(
      padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: bottomBorderColor
              )
          )
      ),
      child: TextField(
          obscureText: isPassword,
          style: TextStyle(
              color: textColor
          ),
          controller: textController,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: text,
              hintStyle: TextStyle(
                  color: bottomBorderColor
              )
          )
      ),
    );
  }
}
