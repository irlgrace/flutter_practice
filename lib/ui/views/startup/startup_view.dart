import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'startup_viewmodel.dart';
import 'dart:async';

class StartUpView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StartUpViewModel>.reactive(
        onModelReady: (model) {
          Timer(Duration(seconds: 2), () => model.handleStartUpLogic());
        },
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.indigo[500],
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                    'Flutter Practice',
                    style: TextStyle(
                        fontSize: 40.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                    ),
                ),
                SizedBox(height: 30.0),
                CircularProgressIndicator(
                  strokeWidth: 3,
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                )
              ],
            ),
          ),
        ),
        viewModelBuilder: () => StartUpViewModel());
  }
}
