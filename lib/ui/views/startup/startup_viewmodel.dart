import 'dart:io';

import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:flutterpractice/models/device.dart';
import 'package:flutterpractice/services/authentication_service.dart';
import 'package:flutterpractice/services/firestore_service.dart';
import 'package:flutterpractice/services/push_notification_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class StartUpViewModel extends BaseViewModel {
  final AuthenticationService _authenticationService = locator<AuthenticationService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final PushNotificationService _pushNotificationService = locator<PushNotificationService>();
  final FirestoreService _firestoreService = locator<FirestoreService>();

  Future handleStartUpLogic() async {
    await _pushNotificationService.initialise();
    var deviceToken = await _pushNotificationService.getToken();
    _firestoreService.addDevice(Device(
      deviceToken: '$deviceToken',
      type: Platform.isAndroid? 'Android' : 'IOS'
    ));
    var hasLoggedInUser = await _authenticationService.isUserLoggedIn();
    if (hasLoggedInUser) {
      _navigationService.replaceWith(Routes.homeViewRoute);
    } else {
      _navigationService.replaceWith(Routes.loginViewRoute);
    }
  }
}
