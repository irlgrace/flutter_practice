import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterpractice/models/post.dart';
import 'package:flutterpractice/ui/views/post/post_viewmodel.dart';
import 'package:flutterpractice/ui/views/reusable_widget/input_field.dart';
import 'package:stacked/stacked.dart';

class PostView extends StatelessWidget {
  final _titleController = TextEditingController();
  final Post editingPost;
  PostView({Key key, this.editingPost}):super (key: key);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<PostViewModel>.reactive(
        onModelReady: (model) {
          _titleController.text = editingPost?.title?? '';
          model.setEditingPost(editingPost);
        },
        builder: (context, model, child) => Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            title: Text('Flutter Practice'),
          ),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
              child: !model.isBusy ? Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 40.0),
                    TextFormField(
                        controller: _titleController,
                        decoration: InputDecoration(
                            hintText: 'Title',
                        ),
                      validator: (value) {
                          if (value.isEmpty) {
                            return 'Title is required';
                          }
                          return null;
                      },
                    ),
                    SizedBox(height: 40.0),
                    Row(
                      children: <Widget>[
                        Text('Post Image'),
                        SizedBox(width: 20.0),
                        model.hasImage?
                        Container():
                        Text(
                          'Image is required',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.red
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 20.0),
                    GestureDetector(
                      onTap: () => model.selectImage(),
                      child: Container(
                        height: 250,
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)
                        ),
                        alignment: Alignment.center,
                        child: (model.selectedImage != null)
                        ? Image.file(model.selectedImage)
                        : ( editingPost != null && editingPost.imageUrl != null
                            ? CachedNetworkImage(
                                imageUrl: editingPost.imageUrl,
                                placeholder: (context, url) => Center(
                                  child: CircularProgressIndicator(),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error)
                            )
                            : Text(
                                  'Tap to add post image',
                                  style: TextStyle(color: Colors.grey[400])
                            )
                        ),
                      ),
                    ),
                    IconButton(
                      onPressed: () => model.captureImage(),
                      icon: Icon(Icons.camera_alt)
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            if(!model.isBusy){
                              if (_formKey.currentState.validate() && model.hasImage) {
                                model.addPost(title: _titleController.text);
                              }
                            }
                          },
                          child: Text('Post',
                            style: TextStyle(
                              color: Colors.white
                            ),
                          ),
                          color: Colors.blue
                        ),
                      ],
                    ),
                  ],
                ),
              )
              : Container(
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor:AlwaysStoppedAnimation(Colors.blue),
                  ),
                ),
              ),
            ),
          ),
        ),
        viewModelBuilder: () => PostViewModel()
    );
  }
}
