import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/models/user.dart';
import 'package:flutterpractice/services/authentication_service.dart';
import 'package:flutterpractice/services/cloud_storage_service.dart';
import 'package:flutterpractice/services/firestore_service.dart';
import 'package:flutterpractice/utils/image_selector.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:flutterpractice/models/post.dart';
import 'package:flutterpractice/models/notification.dart';

class PostViewModel extends BaseViewModel{
  final FirestoreService _firestoreService = locator<FirestoreService>();
  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final AuthenticationService _authenticationService = locator<AuthenticationService>();
  final ImageSelector _imageSelector = locator<ImageSelector>();
  final CloudStorageService _cloudStorageService = locator<CloudStorageService>();


  User get currentUser => _authenticationService.currentUser;

  Post _editingPost;

  File _selectedImage;
  File get selectedImage => _selectedImage;

  bool _hasImage = false;
  bool get hasImage => _hasImage;

  void setEditingPost(Post post) {
    _editingPost = post;
    if(post != null) {
      _hasImage = true;
    }
  }

  bool get _editing => _editingPost != null;

  Future addPost({@required String title}) async {
    setBusy(true);

    CloudStorageResult storageResult;
    String action = 'add';
    if (!_editing || _selectedImage != null) {
      storageResult = await _cloudStorageService.uploadImage(
          imageToUpload: _selectedImage,
          title: title
      );
    }

    var result;
    var notifResult;

    if (!_editing) {
      result = await _firestoreService.addPost(Post(
          title: title,
          userId: currentUser.id,
          imageUrl: storageResult.imageUrl,
          imageFileName: storageResult.imageFileName
      ));
      if(result['uniqueId'] != null) {
        notifResult = await _firestoreService.addNotification(PushNotification(
            title: 'New Created Post',
            body: title,
            message: 'View New Created Post',
            view: 'single_post',
            postId: result['uniqueId']
        ));
      }
    } else {
      if (_selectedImage != null) {
        result = await _firestoreService.updatePost(Post(
            title: title,
            userId: _editingPost.userId,
            documentId: _editingPost.documentId,
            imageUrl: storageResult.imageUrl,
            imageFileName: storageResult.imageFileName
        ));
        _cloudStorageService.deleteImage(_editingPost.imageFileName);

      } else {
        result = await _firestoreService.updatePost(Post(
            title: title,
            userId: _editingPost.userId,
            documentId: _editingPost.documentId,
            imageUrl:  _editingPost.imageUrl,
            imageFileName: _editingPost.imageFileName
        ));
      }
      action = 'edit';
    }

    setBusy(false);

    if (result is String) {
      await _dialogService.showDialog(
        title: 'Could not $action Post',
        description: result
      );
    } else {
      await _dialogService.showDialog(
          title: 'Post successfully $action'+'ed',
          description: 'Your post has been $action'+'ed',
      );
      //await _messagingService.sendAndRetrieveMessage();
    }

    _navigationService.back();
  }

  Future selectImage() async {
    var tempImage = await _imageSelector.selectImage();
    if (tempImage != null) {
      _selectedImage = tempImage;
      _hasImage = true;
      notifyListeners();
    }
  }

  Future captureImage() async {
    var tempImage = await _imageSelector.captureImage();
    if (tempImage != null) {
      _selectedImage = tempImage;
      _hasImage = true;
      notifyListeners();
    }
  }
}