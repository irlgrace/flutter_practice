import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterpractice/ui/views/reusable_widget/custom_app_bar.dart';
import 'package:stacked/stacked.dart';
import 'singlepost_viewmodel.dart';

class SinglePostView extends StatelessWidget {
  final String postId;

  SinglePostView({Key key, this.postId}):super (key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SinglePostViewModel>.reactive(
        onModelReady: (model) async {
          await model.setPost(postId);
        },
        builder: (context, model, child) => Scaffold(
          appBar: new AppBar(
            title: Text('Flutter Practice'),
          ),
          body: Center(
            child: model.post != null
                ? Container(
                      height: model.post.imageUrl != null ? null : 60,
                      margin: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5.0),
                          boxShadow: [
                            BoxShadow(blurRadius: 8, color: Colors.grey[200], spreadRadius: 3.0)
                          ]
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.only(left:15.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(model.post.title == null? ''
                                            ' ': model.post.title,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold
                                            )
                                        ),
                                      ),
                                      model.post.imageUrl != null ?
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                                        child: SizedBox(
                                            height: 250,
                                            child: CachedNetworkImage(
                                                imageUrl: model.post.imageUrl,
                                                placeholder: (context, url) => Center(
                                                  child: CircularProgressIndicator(),
                                                ),
                                                errorWidget: (context, url, error) => Icon(Icons.error)
                                            )
                                        ),
                                      )
                                          : Container()
                                    ],
                                  )
                              )
                          ),
                          Column(
                            children: <Widget>[
                              IconButton(
                                  icon: Icon(Icons.close),
                                  onPressed: () {}
                              ),
                              IconButton(
                                icon: Icon(Icons.edit),
                                onPressed: (){},
                              )
                            ],
                          )
                        ],
                      )
                  )
                 : Container(
                  child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(
                            Theme.of(context).primaryColor
                        )
                      )
                  )
                )
          ),

        ),
        viewModelBuilder: () => SinglePostViewModel());
  }
}
