
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/models/post.dart';
import 'package:flutterpractice/services/firestore_service.dart';
import 'package:stacked/stacked.dart';

class SinglePostViewModel extends BaseViewModel {
  final FirestoreService _firestoreService = locator<FirestoreService>();
  Post _post;
  Post get post => _post;

  Future setPost(id) async {
    setBusy(true);
    _post = await _firestoreService.getSpecificPost(id);
    setBusy(false);
  }
}
