import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:flutterpractice/models/notification.dart';
import 'package:flutterpractice/models/post.dart';
import 'package:flutterpractice/services/cloud_storage_service.dart';
import 'package:flutterpractice/services/firestore_service.dart';
import 'package:flutterpractice/services/push_notification_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HomeViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final FirestoreService _firestoreService = locator<FirestoreService>();
  final DialogService _dialogService = locator<DialogService>();
  final CloudStorageService _cloudStorageService = locator<CloudStorageService>();
  final PushNotificationService _pushNotificationService = locator<PushNotificationService>();
  String _title = 'Home View';
  String get title => _title;

  List<Post> _posts;
  List<Post> get posts => _posts;

  List<PushNotification> _pushNotifications;
  List<PushNotification> get pushNotifications => _pushNotifications;

  Future getPosts() async {
    setBusy(true);
    var postsResults = await _firestoreService.getPostsOnceOff();
    _firestoreService.listenToNotificationRealTime().listen((notificationData) {
      List<PushNotification> newNotif = notificationData;
      if (newNotif != null && newNotif.length > 0) {
        _pushNotifications = newNotif;
        notifyListeners();
      }
    });

    setBusy(false);

    if (postsResults is List<Post>) {
      _posts = postsResults;
      _pushNotificationService.getToken();
      notifyListeners();
    } else {
      await _dialogService.showDialog(
        title: 'Cannot Retrieved Posts',
        description:postsResults
      );
    }
  }

  Future navigateToCreatePost() async  {
    await _navigationService.navigateTo(Routes.postViewRoute);
  }

  Future deletePost(int index) async {
    var dialogResponse = await _dialogService.showConfirmationDialog(
      title: 'Are you sure?',
      description: 'Do you really want to delete the post?',
      confirmationTitle: 'Yes',
      cancelTitle: 'No'
    );

    if (dialogResponse.confirmed) {
      setBusy(true);
      await _firestoreService.deletePost(_posts[index].documentId);
      await _cloudStorageService.deleteImage(_posts[index].imageFileName);
      setBusy(false);
    }
  }

  Future editPost(int index) async {
    await _navigationService.navigateTo(Routes.postViewRoute, arguments: _posts[index]);
  }

  void listenPosts() async {
    setBusy(true);
    _firestoreService.listenToPostsRealTime().listen((postsData){
      List<Post> updatedPosts = postsData;
      if (updatedPosts != null && updatedPosts.length > 0) {
        _posts = updatedPosts;
        notifyListeners();
      }

    });
    setBusy(false);
  }

  void viewSinglePost(String id) async {
    await _navigationService.navigateTo(Routes.singlePostViewRoute, arguments: id);
  }
}
