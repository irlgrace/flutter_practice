import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutterpractice/ui/views/reusable_widget/custom_app_bar.dart';
import 'package:stacked/stacked.dart';
import 'home_viewmodel.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
        onModelReady: (model) => model.listenPosts(),
        builder: (context, model, child) => Scaffold(
          appBar: PreferredSize(
              preferredSize: const Size(double.infinity, kToolbarHeight),
              child: CustomAppBar()
          ),
          body: Center(
            child: model.posts != null ? ListView.builder(
                itemCount: model.posts.length,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () { model.viewSinglePost(model.posts[index].documentId);},
                  child: Container(
                    height: model.posts[index].imageUrl != null ? null : 60,
                    margin: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5.0),
                      boxShadow: [
                        BoxShadow(blurRadius: 8, color: Colors.grey[200], spreadRadius: 3.0)
                      ]
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left:15.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(model.posts[index].title == null? ''
                                        ' ': model.posts[index].title,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      )
                                    ),
                                  ),
                                  model.posts[index].imageUrl != null ?
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                                    child: SizedBox(
                                      height: 250,
                                      child: CachedNetworkImage(
                                        imageUrl: model.posts[index].imageUrl,
                                        placeholder: (context, url) => Center(
                                          child: CircularProgressIndicator(),
                                        ),
                                        errorWidget: (context, url, error) => Icon(Icons.error)
                                      )
                                    ),
                                  )
                                  : Container()
                                ],
                              )
                            )
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () => model.deletePost(index)

                            ),
                            IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () => model.editPost(index),
                            )
                          ],
                        )
                      ],
                    )
                  ),
                )
            ):
            Container(
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                    Theme.of(context).primaryColor
                  ),
                )
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () => model.navigateToCreatePost(),
              child:Icon(
                Icons.add
              )
          ),
        ),
        viewModelBuilder: () => HomeViewModel());
  }
}
