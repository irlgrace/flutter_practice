import 'package:flutter/material.dart';
import 'package:flutterpractice/models/notification.dart';
import 'package:stacked/stacked.dart';
import 'notification_viewmodel.dart';

class NotificationView extends StatelessWidget {
  final List<PushNotification> notifications;
  NotificationView({Key key, this.notifications}):super (key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<NotificationViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
          appBar: new AppBar(
            title: Text('Flutter Practice Notification'),
          ),
          body: ListView.builder(
              itemCount: notifications == null? 0 : notifications.length,
              itemBuilder: (context, index){
                return GestureDetector(
                  onTap: () {
                    model.navigateToSinglePost(notifications[index].postId);
                  },
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text(
                        notifications[index].body,
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black
                        )
                      )
                    ),
                  ),
                );
                }
              )
          ),
        viewModelBuilder: () => NotificationViewModel());
  }
}
