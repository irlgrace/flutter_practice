
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class NotificationViewModel extends BaseViewModel {
  List _notifications;
  List get notifications => _notifications;

  NavigationService _navigationService = locator<NavigationService>();

  void navigateToSinglePost(String postId)  {
     _navigationService.navigateTo(Routes.singlePostViewRoute, arguments: postId);
  }
}
