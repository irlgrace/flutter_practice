import 'package:flutter/foundation.dart';
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:flutterpractice/services/authentication_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class SignUpViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final DialogService _dialogService = locator<DialogService>();
  final AuthenticationService _authenticationService = locator<AuthenticationService>();

  String _title = 'Sign Up';
  String get title => _title;

  Future signUp({
    @required String firstName,
    @required String lastName,
    @required String email,
    @required String password
  }) async {
    setBusy(true);
    var result = await _authenticationService.signUpWithEmail(
        firstName: firstName,
        lastName: lastName,
        email: email.trim(),
        password: password
    );

    setBusy(false);

    if (result is bool) {
      if (result) {
        _navigationService.replaceWith(Routes.homeViewRoute);
      } else {
        await _dialogService.showDialog(
          title: 'Sign Up Failure',
          description: 'Failed to Sign Up. Please try again.'
        );
      }
    } else {
      await _dialogService.showDialog(
          title: 'Sign Up Failure 2',
          description: result
      );
    }
  }

  Future navigateToLogin() async {
    await _navigationService.navigateTo(Routes.loginViewRoute);
  }
}
