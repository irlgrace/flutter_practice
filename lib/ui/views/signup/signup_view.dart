import 'package:flutter/material.dart';
import 'package:flutterpractice/ui/views/reusable_widget/input_field.dart';
import 'package:stacked/stacked.dart';
import 'signup_viewmodel.dart';

class SignUpView extends StatelessWidget {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SignUpViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.indigo[500],
          body: SafeArea(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 60.0),
                      Text(
                        model.title,
                        style: TextStyle(
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                        ),
                      ),
                      SizedBox(height: 40.0),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              InputField(
                                textController: _firstNameController,
                                text: 'First Name',
                                textColor: Colors.white,
                                textType: 'text',
                                bottomBorderColor: Colors.white70,
                              ),
                              SizedBox(height: 40.0),
                              InputField(
                                textController: _lastNameController,
                                text: 'Last Name',
                                textColor: Colors.white,
                                textType: 'text',
                                bottomBorderColor: Colors.white70,
                              ),
                              SizedBox(height: 40.0),
                              InputField(
                                textController: _emailController,
                                text: 'Email',
                                textColor: Colors.white,
                                textType: 'text',
                                bottomBorderColor: Colors.white70,
                              ),
                              SizedBox(height: 40.0),
                              InputField(
                                textController: _passwordController,
                                text: 'Password',
                                textColor: Colors.white,
                                textType: 'password',
                                bottomBorderColor: Colors.white70,
                              ),
                              SizedBox(height: 25.0),
                              GestureDetector(
                                  onTap: () {
                                      model.signUp(
                                          firstName: _firstNameController.text,
                                          lastName: _lastNameController.text,
                                          email: _emailController.text,
                                          password: _passwordController.text
                                      );
                                  },
                                  child: Container(
                                    height: 50.0,
                                    margin: EdgeInsets.symmetric(horizontal: 60),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50.0),
                                        color: Colors.blue[700]
                                    ),
                                    child: Center(
                                        child: Text(
                                            'Sign Up',
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white70
                                            )
                                        )
                                    )
                                )
                              ),
                              SizedBox(height: 20.0),
                              GestureDetector(
                                onTap: () {
                                  model.navigateToLogin();
                                },
                                child: Center(
                                  child: Text(
                                      "Already have account? Login",
                                      style: TextStyle(color: Colors.lightBlue)
                                  ),
                                ),
                              ),
                            ],
                          )
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        viewModelBuilder: () => SignUpViewModel());
  }
}
