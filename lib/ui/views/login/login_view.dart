import 'package:flutter/material.dart';
import 'package:flutterpractice/ui/views/reusable_widget/input_field.dart';
import 'package:stacked/stacked.dart';
import 'login_viewmodel.dart';

class LoginView extends StatelessWidget {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LoginViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.indigo[500],
          body: SafeArea(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 60.0),
                      Text(
                        'Log in',
                        style: TextStyle(
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                        ),
                      ),
                      SizedBox(height: 40.0),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              InputField(
                                textController: _emailController,
                                text: 'Email',
                                textColor: Colors.white,
                                textType: 'text',
                                bottomBorderColor:
                                Colors.white70,
                              ),
                              SizedBox(height: 40.0),
                              InputField(
                                textController: _passwordController,
                                text: 'Password',
                                textColor: Colors.white,
                                textType: 'password',
                                bottomBorderColor: Colors.white70,
                              ),
                              SizedBox(height: 25.0),
                              /*Center(
                                child: Text(
                                    "Forgot Password?",
                                    style: TextStyle(color: Colors.lightBlue)
                                ),
                              ),
                              SizedBox(height: 20.0),*/
                              GestureDetector(
                                  onTap: () {
                                      model.login(
                                          email: _emailController.text,
                                          password: _passwordController.text
                                      );
                                  },
                                  child: Container(
                                    height: 50.0,
                                    margin: EdgeInsets.symmetric(horizontal: 60),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50.0),
                                        color: Colors.blue[700]
                                    ),
                                    child: Center(
                                        child: Text(
                                            model.title,
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white70
                                            )
                                        )
                                    )
                                )
                              ),
                              SizedBox(height: 20.0),
                              GestureDetector(
                                onTap: () {
                                  model.navigateToSignUp();
                                },
                                child: Center(
                                  child: Text(
                                      "Create an account? Sign Up",
                                      style: TextStyle(color: Colors.lightBlue)
                                  ),
                                ),
                              ),
                            ],
                          )
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        viewModelBuilder: () => LoginViewModel());
  }
}
