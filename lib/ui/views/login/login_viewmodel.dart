import 'package:flutter/foundation.dart';
import 'package:flutterpractice/app/locator.dart';
import 'package:flutterpractice/app/router.gr.dart';
import 'package:flutterpractice/services/authentication_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class LoginViewModel extends BaseViewModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final DialogService _dialogService = locator<DialogService>();
  final AuthenticationService _authenticationService = locator<AuthenticationService>();

  String _title = 'Login';
  String get title => _title;

  Future login({@required String email, @required String password}) async {
    setBusy(true);

    var result = await _authenticationService.loginWithEmail(email: email.trim(), password: password);

    setBusy(false);

    if (result is bool) {
      if (result) {
        _navigationService.replaceWith(Routes.homeViewRoute);
      } else {
        await _dialogService.showDialog(
          title: 'Login Failure',
          description: 'Couldn\'t login at this moment. Please try again.'
        );
      }
    } else {
      await _dialogService.showDialog(
          title: 'Login Failure',
          description: result
      );
    }
  }

  Future navigateToSignUp() async {
    await _navigationService.navigateTo(Routes.signUpViewRoute);
  }
}
